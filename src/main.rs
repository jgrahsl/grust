use std::env;
use git2::Repository;

fn delete_tag(r : &git2::Repository, t : &str) {
    match r.tag_delete(&t) {
        Ok(_) => {},
        Err(e) => println!("Error while deleting tag {} {}", t, e),
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let filename = &args[1];
    
    let repo = match Repository::open(filename) {
        Ok(repo) => repo,
        Err(e) => panic!("failed to open: {}", e),
    };
    
    let tags = repo.tag_names(Some("v*dev*")).unwrap();
    for tag in tags.iter() {
        match tag {
            Some(tag_name) => {
                println!("found tag: {}", tag_name);
                delete_tag(&repo, &tag_name);
            },                
            None => println!("empty tag")            
        }
    }
}
